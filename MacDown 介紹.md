# MacDown 介紹  

## Markdown 是什麼？ 


Markdown的目標是實現「易讀易寫」。

**Markdown** 是一種純文字的語法，制定人是John Gruber，此語法的目的是讓語法簡單且容易閱讀和撰寫，詳細的語法請參考此[連結](http://daringfireball.net/projects/markdown/syntax).

**Markdown** 的語法全由標點符號所組成，並經過嚴謹慎選，是為了讓它們看起來就像所要表達的意思。像是在文字兩旁加上星號，看起來就像*強調*。Markdown的清單看起來，嗯，就是清單。假如你有使用過電子郵件，區塊引言看起來就真的像是引用一段文字。

由於此語法為輕量化的標記語言，可以很方便地做出容易閱讀的筆記，且目前已經被多數的前端網站所使用，比如說GitHub這個目前最大的開源版本控管伺服器，標準配備了MarkDown語法的Readme，作為專案的文件語言。

* 撰寫方便快速且原文閱讀性高
* 可以快速轉換成其他格式PDF, Html
* 相容性、移植性高、顯示格式的調整彈性極高

develop

## 語法介紹


### Paragraphs
強制換行只需要在句尾加上兩個空白。

	These lines won't break

	These lines(space)(space)
	will break
edit online

***
### Character styles

**粗體**: `**粗體**` or `__粗體__` 

*強調*: `*強調*` or `_強調_`

***
### Header

	# Header 1
	## Header 2
	### Header 3
	#### Header 4
	##### Header 5
	###### Header 6

***
### Linker

#### [Inline style]
前後加上大小括弧即可: <uranusjr@gmail.com>  
`<uranusjr@gmail.com>`  

URL也是如此: <http://macdown.uranusjr.com>  
` <http://macdown.uranusjr.com>`  

如果想改變連結文字也可以這麼寫: [點我點我](http://123.com)  
`[點我點我](http://123.com)`

#### [Reference style]
如果想要統一控管Link的網址，可以使用參考的方式連結。

[連結名稱][Link 1]  
[Link 1]: http://123.com

原始內容：  
`[連接名稱][Link1]`   
`[Link 1]: http://123.com`

***
### 圖片
#### [Inline style]

![Alt Image Text](http://www.syntecclub.com.tw/img/Syntec.png)

語法：

`![Alt Image Text](http://www.syntecclub.com.tw/img/Syntec.png)`
  
  
#### [Reference style] 

![Alt Image Text][image-id]

[image-id]: http://www.syntecclub.com.tw/img/Syntec.png

語法：

`![Alt Image Text][image-id]`  
`[image-id]: http://www.syntecclub.com.tw/img/Syntec.png`


***
### List

* 沒有順序的項目可以使用 `*` 或 `-` 項目
	* 使用tab可以直接縮排
		1. 也可以使用有順序的項目.
		3. 聽說不用照順序排
		2. ...

原始內容:  

```
* 沒有順序的項目可以使用 `*` 或 `-` 項目
	* 使用tab可以直接縮排
		1. 也可以使用有順序的項目.
		3. 聽說不用照順序排
		2. ...
```
  
***  
### Quate

> 在該行前使用大於的符號 `>` 可以變成引用的格式
> > 引用的區塊可以兩層  
> > > 或者三層
>
> 在引用的區塊內一樣可以使用列表、連結等語法。
>
> * Lists
> * [Links][Link 1]
> * Etc.

原始內容:

```
> 在該行前使用大於的符號 `>` 可以變成引用的格式
> > 引用的區塊可以兩層  
> > > 或者三層
>
> 在引用的區塊內一樣可以使用列表、連結等語法。
>
> * Lists
> * [Links][Link 1]
> * Etc.
```
  
***
### Inline Code

如果要標記一小段行內程式碼，你可以用反引號`` ` ``把它包起來，例如：

`` ` Inline code ` ``

如果那段文字裡面也存在反引號，可以用雙反引號```` `` ````把它包來，例如：  
```` ``code has `backticks` `` ````

***
### Block Code
要在Markdown中建立程式碼區塊很簡單，只要簡單地縮排4個空白或是1個tab就可以，
例如下方的內容：

	這裡是程式區塊
	可以使用四個空白或是1個tab
		在區塊內的縮排也是可行的    
  
或是使用三個反引號(\`\`\`)把內容包裝起來：

```
這裡是程式區塊
可以使用四個空白或是1個tab
	在區塊內的縮排也是可行的
```

***
### Horizontal Rules

要建立分隔線可以使用 `***` 或是 `---` 


***
### Table (非標準Markdown提供)

範例一:

第一欄  | 第二欄
------ | -----
內容    | 內容
內容    | 內容

原始內容：

```
第一欄  | 第二欄
------ | -----
內容    | 內容
內容    | 內容
```

範例二:

| 靠左          | 置中      | 靠右          |
|:------------ |:---------:| ------------:|
| 第一列         | Colingsta | Luffy       |
| 第二列         | is        | write       |
| 第三列來點空白  | godlike   | the document |

```
| left          | center    | right        |
|:------------- |:---------:| ------------:|
| 第一列         | Colingsta | Luffy        |
| 第二列         | is        | write        |
| 第三列來點空白  | Godlike   | the document |
```

##參考連結

語法介紹：

* [GitBucket for markdown](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
* [MarkDown](http://markdown.tw)

工具下載：

* [MacDown (Mac)](http://macdown.uranusjr.com/)
* [Mou (Mac)](http://25.io/mou/)
* [Markdown Pad (Windows)](http://markdownpad.com)
* [StackEdit (Online)](https://stackedit.io/editor)
* [Dillinger.io (Online)](dillinger.io)
